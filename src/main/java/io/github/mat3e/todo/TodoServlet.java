package io.github.mat3e.todo;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "Todo", urlPatterns = {"/api/todos/*"})
public class TodoServlet extends HttpServlet {

    private final Logger logger = LoggerFactory.getLogger(TodoServlet.class);

    private TodoRepository service;
    private ObjectMapper mapper;

    /***
     * Servlet container needs it.
     */
    @SuppressWarnings("unused")
    public TodoServlet() {
        this(new TodoRepository(), new ObjectMapper());
    }

    TodoServlet(TodoRepository service, ObjectMapper mapper) {
        this.mapper = mapper;
        this.service = service;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // req.setCharacterEncoding("UTF-8");
        // resp.setCharacterEncoding("UTF-8");
        resp.setContentType("text/plain; charset=utf-8");
        logger.info("Got request with parameters " + req.getParameterMap());

        resp.setContentType("application/json;charset-UTF-8");
        mapper.writeValue(resp.getOutputStream(), service.findAll());


        //resp.getWriter().write(service.prepareGreetings(name, lang));
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        var path = req.getPathInfo().substring(1);

        try {

            var pathId = Integer.valueOf(path);
            resp.setContentType("application/json;charset-UTF-8");
            mapper.writeValue(resp.getOutputStream(), service.toggleTodo(pathId));


        } catch (NumberFormatException e) {

            logger.warn("WRONG PATH used: " + path);
        }
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setContentType("application/json;charset-UTF-8");
        var newTodo = mapper.readValue(req.getInputStream(), Todo.class);
        mapper.writeValue(resp.getOutputStream(), service.addTodo(newTodo));
    }
}
