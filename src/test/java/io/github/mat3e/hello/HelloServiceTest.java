package io.github.mat3e.hello;

import io.github.mat3e.lang.Lang;
import io.github.mat3e.lang.LangRepository;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertEquals;

public class HelloServiceTest {

    private static final String WELCOME = "Hello";
    private static final String FALLBACK_ID_WELCOME = "Hola";


    @Test
    public void test_prepareGreetings_nullName_returns_GreetingWithFallbackName() throws Exception {

        //given
        var mockRepository = alwaysReturningHelloRepository();

        var SUT = new HelloService(mockRepository);
        // when
        var result = SUT.prepareGreetings(null, -1);

        //then
        assertEquals(WELCOME + " " + HelloService.FALLBACK_NAME + "!", result);
    }


    @Test
    public void test_prepareGreetings_name_returns_GreetingWithName() throws Exception {

        //given
        var mockRepository = alwaysReturningHelloRepository();
        var name = "test";
        var SUT = new HelloService(mockRepository);

        // when
        var result = SUT.prepareGreetings(name, -1);

        //then
        assertEquals(WELCOME + " " + name + "!", result);
    }


    @Test
    public void test_prepareGreeting_nonExistingLang_returnsGreetingWithFallbackLang() throws Exception {

        //given
        var mockEmptyRepository = new LangRepository() {

            @Override
            public Optional<Lang> findById(Integer id) {
                return Optional.empty();
            }
        };

        var SUT = new HelloService(mockEmptyRepository);

        // when
        var result = SUT.prepareGreetings(null, -1);

        //then
        assertEquals(HelloService.FALLBACK_LANG.getWelcomeMsg() + " " + HelloService.FALLBACK_NAME + "!", result);
    }


    @Test
    public void test_prepareGreetings_nullLang_returns_GreetingWithFallbackIdLang() {

        //given
        var mockRepository = fallbackLangIdRepository();

        var name = "test";
        var SUT = new HelloService(mockRepository);

        // when
        var result = SUT.prepareGreetings(null, null);

        //then
        assertEquals(FALLBACK_ID_WELCOME + " " + HelloService.FALLBACK_NAME + "!", result);
    }

//    @Test
//    public void test_prepareGreetings_textLang_returns_GreetingWithFallbackIdLang() {
//
//        //given
//        var mockRepository = fallbackLangIdRepository();
//
//        var name = "test";
//        var SUT = new HelloService(mockRepository);
//
//        // when
//        var result = SUT.prepareGreetings(null, "abc");
//
//        //then
//        assertEquals(FALLBACK_ID_WELCOME + " " + HelloService.FALLBACK_NAME + "!", result);
//    }


    private LangRepository fallbackLangIdRepository() {
        return new LangRepository() {
            @Override
            public Optional<Lang> findById(Integer id) {

                if (id.equals(HelloService.FALLBACK_LANG.getId())) {
                    return Optional.of(new Lang(null, FALLBACK_ID_WELCOME, null));
                }

                return Optional.empty();

            }
        };
    }

    private LangRepository alwaysReturningHelloRepository() {
        return new LangRepository() {

            @Override
            public Optional<Lang> findById(Integer id) {
                return Optional.of(new Lang(null, WELCOME, null));
            }
        };
    }
}
